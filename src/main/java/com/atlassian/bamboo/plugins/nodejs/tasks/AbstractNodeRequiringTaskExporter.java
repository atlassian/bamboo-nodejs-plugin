package com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.BaseNodeTask;
import com.atlassian.bamboo.specs.model.task.BaseNodeTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract Bamboo Specs exporter for Node.js requiring tasks. Specs properties for applicable tasks should extend
 * {@link BaseNodeTaskProperties}.
 *
 * @param <P> type of specs properties that this task operates on
 * @param <B> type of specs builder that this task operates on
 * @see AbstractNodeRequiringTaskConfigurator
 * @see BaseNodeTaskProperties
 * @see BaseNodeTask
 */
public abstract class AbstractNodeRequiringTaskExporter<P extends BaseNodeTaskProperties, B extends BaseNodeTask<B, P>> implements TaskDefinitionExporter {
    private final Class<P> propertiesClass;
    private final UIConfigSupport uiConfigSupport;

    protected AbstractNodeRequiringTaskExporter(Class<P> propertiesClass, UIConfigSupport uiConfigSupport) {
        this.propertiesClass = propertiesClass;
        this.uiConfigSupport = uiConfigSupport;
    }

    /**
     * Validation context of this task type.
     */
    @NotNull
    protected abstract ValidationContext getValidationContext();

    /**
     * Return task configuration for exporting. Should only include fields which are not part of
     * {@link BaseNodeTaskProperties}.
     */
    @NotNull
    protected abstract Map<String, String> toTaskConfiguration(@NotNull P taskProperties);

    @NotNull
    @Override
    public final Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties) {
        final P typedTaskProperties = Narrow.downTo(taskProperties, propertiesClass);
        Preconditions.checkState(typedTaskProperties != null, "Don't know how to import task properties of type: " + taskProperties.getClass().getName());

        final Map<String, String> config = new HashMap<>();
        config.put(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME, typedTaskProperties.getNodeExecutable());
        config.put(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, typedTaskProperties.getEnvironmentVariables());
        config.put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, typedTaskProperties.getWorkingSubdirectory());

        config.putAll(toTaskConfiguration(typedTaskProperties));

        return config;
    }

    /**
     * Instantiate the specs entity. Only need to set fields specific to the task type, which are not part of
     * {@link BaseNodeTask}.
     */
    @NotNull
    protected abstract B toSpecsEntity(@NotNull Map<String, String> taskConfiguration);

    @NotNull
    @Override
    public final B toSpecsEntity(@NotNull TaskDefinition taskDefinition) {
        final Map<String, String> config = taskDefinition.getConfiguration();
        return toSpecsEntity(config)
                .nodeExecutable(config.get(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME))
                .environmentVariables(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES))
                .workingSubdirectory(config.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY));
    }

    /**
     * Validate the specs entity. Only need to validate fields specific to the task type, which are not part of
     * {@link BaseNodeTaskProperties}.
     */
    @NotNull
    protected abstract List<ValidationProblem> validate(@NotNull P taskProperties);

    @Override
    public final List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();

        final P typedTaskProperties = Narrow.downTo(taskProperties, propertiesClass);
        if (typedTaskProperties != null) {
            final String nodeExecutable = typedTaskProperties.getNodeExecutable();
            if (StringUtils.isEmpty(nodeExecutable)) {
                validationProblems.add(new ValidationProblem(getValidationContext(), "Node.js executable can't be empty"));
            } else {
                final List<String> executableLabels = uiConfigSupport.getExecutableLabels(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_KEY);
                if (executableLabels.stream().noneMatch(label -> label.equals(nodeExecutable))) {
                    final String errorMessage = String.format("Can't find Node.js executable by label: '%s'. Available values: %s", nodeExecutable, executableLabels);
                    validationProblems.add(new ValidationProblem(getValidationContext(), errorMessage));
                }
            }

            validationProblems.addAll(validate(typedTaskProperties));
        }

        return validationProblems;
    }
}
