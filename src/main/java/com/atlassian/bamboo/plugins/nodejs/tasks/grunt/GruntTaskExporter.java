package com.atlassian.bamboo.plugins.nodejs.tasks.grunt;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.GruntTask;
import com.atlassian.bamboo.specs.model.task.GruntTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GruntTaskExporter extends AbstractNodeRequiringTaskExporter<GruntTaskProperties, GruntTask> {
    @Autowired
    public GruntTaskExporter(UIConfigSupport uiConfigSupport) {
        super(GruntTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return GruntTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull GruntTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(GruntConfigurator.GRUNT_RUNTIME, taskProperties.getGruntCliExecutable());
        config.put(GruntConfigurator.TASK, taskProperties.getTask());
        config.put(GruntConfigurator.CONFIG_FILE, taskProperties.getGruntfile());
        return config;
    }

    @NotNull
    @Override
    protected GruntTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new GruntTask()
                .gruntCliExecutable(taskConfiguration.get(GruntConfigurator.GRUNT_RUNTIME))
                .task(taskConfiguration.getOrDefault(GruntConfigurator.TASK, null))
                .gruntfile(taskConfiguration.getOrDefault(GruntConfigurator.CONFIG_FILE, null));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull GruntTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();
        if (StringUtils.isBlank(taskProperties.getGruntCliExecutable())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Grunt CLI executable is not defined"));
        }
        return validationProblems;
    }
}
