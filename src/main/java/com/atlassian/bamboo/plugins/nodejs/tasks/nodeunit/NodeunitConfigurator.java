package com.atlassian.bamboo.plugins.nodejs.tasks.nodeunit;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.specs.builders.task.NodeunitTask;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;

public class NodeunitConfigurator extends AbstractNodeRequiringTaskConfigurator implements TaskTestResultsSupport {
    private static final String NODEUNIT_DEFAULT_EXECUTABLE = NodeunitTask.DEFAULT_NODEUNIT_EXECUTABLE;

    public static final String NODEUNIT_RUNTIME = "nodeunitRuntime";
    public static final String TEST_FILES = "testFiles";
    public static final String TEST_RESULTS_DIRECTORY = "testResultsDir";
    public static final String PARSE_TEST_RESULTS = "parseTestResults";
    public static final String ARGUMENTS = "arguments";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeRequiringTaskConfigurator.FIELDS_TO_COPY)
            .add(NODEUNIT_RUNTIME)
            .add(TEST_FILES)
            .add(TEST_RESULTS_DIRECTORY)
            .add(PARSE_TEST_RESULTS)
            .add(ARGUMENTS)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeRequiringTaskConfigurator.DEFAULT_FIELD_VALUES)
            .put(NODEUNIT_RUNTIME, NODEUNIT_DEFAULT_EXECUTABLE)
            .put(TEST_FILES, NodeunitTask.DEFAULT_TEST_DIRECTORY)
            .put(TEST_RESULTS_DIRECTORY, NodeunitTask.DEFAULT_RESULTS_DIRECTORY)
            .put(PARSE_TEST_RESULTS, NodeunitTask.DEFAULT_PARSE_TEST_RESULTS)
            .build();

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        if (StringUtils.isEmpty(params.getString(NODEUNIT_RUNTIME))) {
            errorCollection.addError(NODEUNIT_RUNTIME, i18nResolver.getText("nodeunit.runtime.error.empty"));
        }
        if (StringUtils.isEmpty(params.getString(TEST_FILES))) {
            errorCollection.addError(TEST_FILES, i18nResolver.getText("nodeunit.testFiles.error.empty"));
        }
        if (StringUtils.isEmpty(params.getString(TEST_RESULTS_DIRECTORY))) {
            errorCollection.addError(TEST_RESULTS_DIRECTORY, i18nResolver.getText("nodeunit.testResultsDir.error.empty"));
        }
    }

    @Override
    public boolean taskProducesTestResults(@NotNull final TaskDefinition taskDefinition) {
        return Boolean.parseBoolean(taskDefinition.getConfiguration().get(PARSE_TEST_RESULTS));
    }

    @NotNull
    @Override
    public Set<String> getFieldsToCopy() {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues() {
        return DEFAULT_FIELD_VALUES;
    }
}
