# Node.js
builder.node.executableName = Node.js
builder.node.helpPath = The path to the Node.js executable (e.g. ''/usr/local/bin/node'')

node.runtime = Node.js executable
node.runtime.error.empty = No Node executable was selected
node.runtime.error.undefinedPath = Node.js path is not defined

node.command = Script
node.command.description = Script to execute with node (e.g. ''server.js'', ''application.js'')
node.command.error.empty = You must specify a script to be executed with node

node.arguments = Arguments
node.arguments.description = Additional command line arguments to pass to node when executing the script.


# npm
npm.command = Command
npm.command.description = Command that npm should run (e.g. ''install'', ''test'')
npm.command.error.empty = You must supply a command for npm to execute

npm.isolatedCache = Use isolated cache
npm.isolatedCache.description = A temporary directory will be used as cache folder. Might slow down task execution.
npm.isolatedCache.usage = Using isolated cache directory: {0}


# Bower
bower.command = Command
bower.command.description = Command that Bower should run (e.g. ''install'')
bower.command.error.empty = You must supply a command for Bower to execute
bower.command.error.undefined = Bower command is not defined

bower.runtime = Bower executable
bower.runtime.description = Specify path to the Bower executable for this task. \
  Path must be relative to the working directory.
bower.runtime.error.empty = You must specify a path to the Bower executable
bower.runtime.error.undefinedPath = Bower path is not defined


# Mocha runner
mocha.runner.runtime = Mocha executable
mocha.runner.runtime.description = Specify path to Mocha executable for this task. Path must be relative to the working directory.
mocha.runner.runtime.error.empty = You must specify a path to Mocha
mocha.runner.runtime.error.undefinedPath = Mocha path is not defined

mocha.runner.testFiles = Test files and directories
mocha.runner.testFiles.description = Specify files and/or directories containing Mocha tests. \
  You can specify multiple files and directories separated by a space.
mocha.runner.testFiles.error.empty = You must specify at least one file or directory containing tests

mocha.runner.parseTestResults = Parse test results produced by this task

mocha.runner.arguments = Arguments
mocha.runner.arguments.description = Additional command line arguments to pass to Mocha. \
  Be aware that some options may already be set by this task configuration.


# Mocha parser
mocha.parser.testPattern = Testfile pattern
mocha.parser.testPattern.description = Ant-style pattern of file to parse.
mocha.parser.testPattern.error.empty = You must specify a file pattern


# Grunt
grunt.runtime = Grunt CLI executable
grunt.runtime.description = Specify path to Grunt command line interface (grunt-cli) executable for this task. \
  Path must be relative to the working directory.
grunt.runtime.error.empty = You must specify a path to Grunt CLI
grunt.runtime.error.undefinedPath = Grunt path is not defined

grunt.task = Task
grunt.task.description = Grunt task to execute. If not specified, the ''default'' task will be executed. \
  You can specify multiple tasks separated by a space.

grunt.configFile = Alternative gruntfile
grunt.configFile.description = Specify path to the gruntfile, relative to the build working directory. \
  Leave blank to use the default.


# Gulp
gulp.runtime = Gulp executable
gulp.runtime.description = Specify path to the Gulp executable for this task. \
  Path must be relative to the working directory.
gulp.runtime.error.empty = You must specify a path to the Gulp executable
gulp.runtime.error.undefinedPath = Gulp path is not defined

gulp.task = Task
gulp.task.description = Gulp task to execute. If not specified, the ''default'' task will be executed. \
  You can specify multiple tasks separated by a space.

gulp.configFile = Alternative gulpfile
gulp.configFile.description = Specify path to the gulpfile, relative to the build working directory. \
  Leave blank to use the default. Only supported for Gulp 3.3.2 or newer.


# Nodeunit
nodeunit.runtime = Nodeunit executable
nodeunit.runtime.description = Specify path to Nodeunit executable for this task. Path must be relative to the working directory.
nodeunit.runtime.error.empty = You must specify a path to Nodeunit
nodeunit.runtime.error.undefinedPath = Nodeunit path is not defined

nodeunit.testFiles = Test files and directories
nodeunit.testFiles.description = Specify files and/or directories containing Nodeunit tests. \
  You can specify multiple files and directories separated by a space.
nodeunit.testFiles.error.empty = You must specify at least one file or directory containing tests

nodeunit.testResultsDir = Results directory
nodeunit.testResultsDir.description = Name of the directory where the test results will be stored (in JUnit XML format).
nodeunit.testResultsDir.error.empty = You must specify a directory where the test results will be stored

nodeunit.parseTestResults = Parse test results produced by this task

nodeunit.arguments = Arguments
nodeunit.arguments.description = Additional command line arguments to pass to Nodeunit. \
  Be aware that some options may already be set by this task configuration.
