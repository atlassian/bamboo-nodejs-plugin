package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NpmTask;
import com.atlassian.bamboo.specs.model.task.NpmTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NpmTaskExporterTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UIConfigSupport uiConfigSupport;

    @InjectMocks
    private NpmTaskExporter npmTaskExporter;

    @Test
    public void testToTaskConfiguration() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String command = "install";
        final boolean useIsolatedCache = true;
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final TaskContainer taskContainer = mock(TaskContainer.class);
        final NpmTask npmTask = new NpmTask()
                .nodeExecutable(nodeExecutable)
                .command(command)
                .useIsolatedCache(useIsolatedCache)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);
        final NpmTaskProperties npmTaskProperties = EntityPropertiesBuilders.build(npmTask);

        // when
        final Map<String, String> configuration = npmTaskExporter.toTaskConfiguration(taskContainer, npmTaskProperties);

        // then
        assertThat(configuration.get(NpmConfigurator.NODE_RUNTIME), is(nodeExecutable));
        assertThat(configuration.get(NpmConfigurator.COMMAND), is(command));
        assertThat(configuration.get(NpmConfigurator.ISOLATED_CACHE), is(Boolean.toString(useIsolatedCache)));
        assertThat(configuration.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), is(environmentVariables));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(workingSubdirectory));
    }

    @Test
    public void testToSpecsEntity() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String command = "install";
        final boolean useIsolatedCache = true;
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final Map<String, String> configuration = ImmutableMap.<String, String>builder()
                .put(NpmConfigurator.NODE_RUNTIME, nodeExecutable)
                .put(NpmConfigurator.COMMAND, command)
                .put(NpmConfigurator.ISOLATED_CACHE, Boolean.toString(useIsolatedCache))
                .put(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, environmentVariables)
                .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory)
                .build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final NpmTaskProperties taskProperties = EntityPropertiesBuilders.build(npmTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getNodeExecutable(), is(nodeExecutable));
        assertThat(taskProperties.getCommand(), is(command));
        assertThat(taskProperties.isUseIsolatedCache(), is(useIsolatedCache));
        assertThat(taskProperties.getEnvironmentVariables(), is(environmentVariables));
        assertThat(taskProperties.getWorkingSubdirectory(), is(workingSubdirectory));
    }

    @Test
    public void testValidatePassesForValidProperties() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};

        final NpmTask npmTask = new NpmTask()
                .nodeExecutable(knownNodeExecutables[0])
                .command("test");
        final NpmTaskProperties npmTaskProperties = EntityPropertiesBuilders.build(npmTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = npmTaskExporter.validate(taskValidationContext, npmTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }

    @Test
    public void testValidateFailsForUnknownExecutable() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};
        final String missingNodeExecutable = knownNodeExecutables[0] + " (missing)";
        final NpmTask npmTask = new NpmTask()
                .nodeExecutable(missingNodeExecutable)
                .command("install");
        final NpmTaskProperties npmTaskProperties = EntityPropertiesBuilders.build(npmTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = npmTaskExporter.validate(taskValidationContext, npmTaskProperties);

        // then
        assertThat(validationProblems, hasSize(1));
    }

    private void registerKnownServerExecutables(@NotNull String... labels) {
        when(uiConfigSupport.getExecutableLabels(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_KEY)).thenReturn(Arrays.asList(labels));
    }
}
