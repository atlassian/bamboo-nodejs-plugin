package com.atlassian.bamboo.plugins.nodejs.tasks.node;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NodeTask;
import com.atlassian.bamboo.specs.model.task.NodeTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NodeTaskExporterTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UIConfigSupport uiConfigSupport;

    @InjectMocks
    private NodeTaskExporter nodeTaskExporter;

    @Test
    public void testToTaskConfiguration() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String script = "application.js";
        final String arguments = "--server";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final TaskContainer taskContainer = mock(TaskContainer.class);

        final NodeTask nodeTask = new NodeTask()
                .nodeExecutable(nodeExecutable)
                .script(script)
                .arguments(arguments)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);
        final NodeTaskProperties nodeTaskProperties = EntityPropertiesBuilders.build(nodeTask);

        // when
        final Map<String, String> configuration = nodeTaskExporter.toTaskConfiguration(taskContainer, nodeTaskProperties);

        // then
        assertThat(configuration.get(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME), is(nodeExecutable));
        assertThat(configuration.get(NodeConfigurator.COMMAND), is(script));
        assertThat(configuration.get(NodeConfigurator.ARGUMENTS), is(arguments));
        assertThat(configuration.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), is(environmentVariables));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(workingSubdirectory));
    }

    @Test
    public void testToSpecsEntity() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String script = "application.js";
        final String arguments = "--server";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final Map<String, String> configuration = ImmutableMap.<String, String>builder()
                .put(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME, nodeExecutable)
                .put(NodeConfigurator.COMMAND, script)
                .put(NodeConfigurator.ARGUMENTS, arguments)
                .put(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, environmentVariables)
                .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory)
                .build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final NodeTaskProperties taskProperties = EntityPropertiesBuilders.build(nodeTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getNodeExecutable(), is(nodeExecutable));
        assertThat(taskProperties.getScript(), is(script));
        assertThat(taskProperties.getArguments(), is(arguments));
        assertThat(taskProperties.getEnvironmentVariables(), is(environmentVariables));
        assertThat(taskProperties.getWorkingSubdirectory(), is(workingSubdirectory));
    }

    @Test
    public void testValidatePassesForValidProperties() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};

        final NodeTask nodeTask = new NodeTask()
                .nodeExecutable(knownNodeExecutables[0])
                .script("server.js");
        final NodeTaskProperties nodeTaskProperties = EntityPropertiesBuilders.build(nodeTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = nodeTaskExporter.validate(taskValidationContext, nodeTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }

    @Test
    public void testValidateFailsForUnknownExecutable() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};
        final String missingNodeExecutable = knownNodeExecutables[0] + " (missing)";
        final NodeTask nodeTask = new NodeTask()
                .nodeExecutable(missingNodeExecutable)
                .script("server.js");
        final NodeTaskProperties nodeTaskProperties = EntityPropertiesBuilders.build(nodeTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = nodeTaskExporter.validate(taskValidationContext, nodeTaskProperties);

        // then
        assertThat(validationProblems, hasSize(1));
    }

    private void registerKnownServerExecutables(@NotNull String... labels) {
        when(uiConfigSupport.getExecutableLabels(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_KEY)).thenReturn(Arrays.asList(labels));
    }
}
