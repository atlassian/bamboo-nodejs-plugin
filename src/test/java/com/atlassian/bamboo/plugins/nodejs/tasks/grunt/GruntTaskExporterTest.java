package com.atlassian.bamboo.plugins.nodejs.tasks.grunt;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.GruntTask;
import com.atlassian.bamboo.specs.model.task.GruntTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GruntTaskExporterTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UIConfigSupport uiConfigSupport;

    @InjectMocks
    private GruntTaskExporter gruntTaskExporter;

    @Test
    public void testToTaskConfiguration() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String task = "test";
        final String gruntfile = "Gruntfile-custom.js";
        final String gruntExecutable = "custom/node_modules/grunt";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final TaskContainer taskContainer = mock(TaskContainer.class);

        final GruntTask gruntTask = new GruntTask()
                .nodeExecutable(nodeExecutable)
                .task(task)
                .gruntfile(gruntfile)
                .gruntCliExecutable(gruntExecutable)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);
        final GruntTaskProperties gruntTaskProperties = EntityPropertiesBuilders.build(gruntTask);

        // when
        final Map<String, String> configuration = gruntTaskExporter.toTaskConfiguration(taskContainer, gruntTaskProperties);

        // then
        assertThat(configuration.get(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME), is(nodeExecutable));
        assertThat(configuration.get(GruntConfigurator.GRUNT_RUNTIME), is(gruntExecutable));
        assertThat(configuration.get(GruntConfigurator.CONFIG_FILE), is(gruntfile));
        assertThat(configuration.get(GruntConfigurator.TASK), is(task));
        assertThat(configuration.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), is(environmentVariables));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(workingSubdirectory));
    }

    @Test
    public void testToSpecsEntity() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String task = "test";
        final String gruntfile = "Gruntfile-custom.js";
        final String gruntExecutable = "custom/node_modules/grunt";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final Map<String, String> configuration = ImmutableMap.<String, String>builder()
                .put(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME, nodeExecutable)
                .put(GruntConfigurator.GRUNT_RUNTIME, gruntExecutable)
                .put(GruntConfigurator.CONFIG_FILE, gruntfile)
                .put(GruntConfigurator.TASK, task)
                .put(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, environmentVariables)
                .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory)
                .build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final GruntTaskProperties taskProperties = EntityPropertiesBuilders.build(gruntTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getNodeExecutable(), is(nodeExecutable));
        assertThat(taskProperties.getGruntCliExecutable(), is(gruntExecutable));
        assertThat(taskProperties.getGruntfile(), is(gruntfile));
        assertThat(taskProperties.getTask(), is(task));
        assertThat(taskProperties.getEnvironmentVariables(), is(environmentVariables));
        assertThat(taskProperties.getWorkingSubdirectory(), is(workingSubdirectory));
    }

    @Test
    public void testValidatePassesForValidProperties() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};

        final GruntTask gruntTask = new GruntTask().nodeExecutable(knownNodeExecutables[0]);
        final GruntTaskProperties gruntTaskProperties = EntityPropertiesBuilders.build(gruntTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = gruntTaskExporter.validate(taskValidationContext, gruntTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }

    @Test
    public void testValidateFailsForUnknownExecutable() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};
        final String missingNodeExecutable = knownNodeExecutables[0] + " (missing)";
        final GruntTask gruntTask = new GruntTask().nodeExecutable(missingNodeExecutable);
        final GruntTaskProperties gruntTaskProperties = EntityPropertiesBuilders.build(gruntTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = gruntTaskExporter.validate(taskValidationContext, gruntTaskProperties);

        // then
        assertThat(validationProblems, hasSize(1));
    }

    private void registerKnownServerExecutables(@NotNull String... labels) {
        when(uiConfigSupport.getExecutableLabels(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_KEY)).thenReturn(Arrays.asList(labels));
    }
}
