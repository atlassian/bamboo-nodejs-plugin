package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.rest.model.agent.RestCapability;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.SimpleUserPasswordCredentials;
import com.atlassian.bamboo.testutils.AcceptanceTestHelper;
import com.atlassian.bamboo.testutils.TestBuildDetailsHelper;
import com.atlassian.bamboo.testutils.backdoor.model.Result;
import com.atlassian.bamboo.testutils.config.BambooEnvironmentData;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.junit.rule.LogTestProgressRule;
import com.atlassian.bamboo.testutils.junit.rule.RepeatRule;
import com.atlassian.bamboo.testutils.junit.rule.RepeatSpec;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.user.TestUser;
import com.atlassian.bamboo.testutils.vcs.git.GitRepositoryDescriptor;
import com.atlassian.bamboo.testutils.vcs.git.LocalGitSetupHelper;
import com.atlassian.bamboo.webdriver.TestInjectionTestRule;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.util.concurrent.LazyReference;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.text.MessageFormat;
import java.util.Properties;

public abstract class AbstractNodeTaskTest {
    protected static final BambooTestedProduct product = BambooTestedProductFactory.create();
    protected static final LazyReference<GitRepositoryDescriptor> repository = new LazyReference<GitRepositoryDescriptor>() {
        @Override
        protected GitRepositoryDescriptor create() throws Exception {
            return LocalGitSetupHelper.createRepositoryFromZip("test-repository.zip");
        }
    };

    private final BambooEnvironmentData environmentData = new WebDriverTestEnvironmentData();

    protected final BackdoorRule backdoor = new BackdoorRule(environmentData);
    private final TestInjectionTestRule injectionRule = new TestInjectionTestRule(this, product);
    private final WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();
    private final LogTestProgressRule logTestProgressRule = new LogTestProgressRule(backdoor.getEnvironmentData());
    private final TearDownRule tearDownRule = new TearDownRule();
    private final RepeatRule repeatRule = new RepeatRule(
            new RepeatSpec().profile("stable").minExecutions(1).maxExecutions(5).passThreshold(0.80),
            new RepeatSpec().profile("stress").minExecutions(5).maxExecutions(5).passThreshold(1.0));

    /**
     * Do not change the order of rules unless there is a good reason for it. Reasons for current order:
     * - repeat rule should wrap everything
     * - backdoor modules should be available for #after methods
     * - screenshot should be taken before #after methods pollute the test with cleanup code
     */
    @Rule
    public final RuleChain ruleChain = RuleChain
            .outerRule(repeatRule)
            .around(backdoor)
            .around(logTestProgressRule)
            .around(tearDownRule)
            .around(screenshotRule)
            .around(injectionRule);

    private final Properties i18nProperties = AcceptanceTestHelper.loadProperties("nodejs.properties");

    /**
     * Method to be executed before each test starts.
     */
    protected abstract void onSetUp();

    /**
     * Method to be executed after each test finishes.
     */
    protected abstract void onTearDown();

    @Before
    public final void setUp() {
        backdoor.serverCapabilities().detectServerCapabilities();
        onSetUp();
    }

    /**
     * Not annotated with {@link After} on purpose - handled by {@link TearDownRule}.
     */
    private void tearDown() {
        onTearDown();
    }

    @NotNull
    protected TestBuildDetails createAndSetupPlan() throws Exception {
        TestBuildDetails plan =
                TestBuildDetailsHelper
                        .createNoRepositoryPlanWithScriptTask()
                        .withNoInitialBuild()
                        .withManualBuild()
                        .build();
        TestBuildDetailsHelper.setupGitPlan(plan, repository.get());
        backdoor.plans().createPlan(plan);
        return plan;
    }

    protected int getTotalNumberOfTestsForBuild(@NotNull PlanKey planKey, int buildNumber) throws Exception {
        final Result buildResult = backdoor.plans().getBuildResult(planKey, buildNumber);
        return buildResult.getSuccessfulTestCount() + buildResult.getFailedTestCount() + buildResult.getQuarantinedTestCount();
    }

    protected int getNumberOfSuccessfulTestsForBuild(@NotNull PlanKey planKey, int buildNumber) throws Exception {
        final Result buildResult = backdoor.plans().getBuildResult(planKey, buildNumber);
        return buildResult.getSuccessfulTestCount();
    }

    @Nullable
    protected String getText(@NotNull String key, Object... args) {
        final String property = i18nProperties.getProperty(key);
        return property != null
                ? MessageFormat.format(property, args)
                : null;
    }

    /**
     * Returns an instance of {@link BambooServer} for sending Bamboo Specs.
     *
     * @param testUser user which will be sending the spec entities
     */
    @NotNull
    protected BambooServer getBambooServer(TestUser testUser) {
        return new BambooServer(
                environmentData.getBaseUrl().toString(),
                new SimpleUserPasswordCredentials(testUser.username, testUser.password));
    }

    /**
     * Returns the label of any Node.js server capability which exists in Bamboo. This method gives no guarantees on
     * the version of Node.js returned (two subsequent calls may return two different Node.js capabilities).
     */
    @NotNull
    protected String getExistingNodeCapabilityLabel() {
        return backdoor.serverCapabilities().getAllCapabilities().stream()
                .filter(restCapability -> restCapability.getKey().startsWith(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX))
                .findFirst()
                .map(RestCapability::getKey)
                .map(key -> StringUtils.substringAfter(key, NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX + "."))
                .orElseThrow(() -> new AssertionError("No Node.js server capability found"));
    }

    /**
     * Take failure screenshot before call of after() logic
     */
    private class TearDownRule implements TestRule {
        @Override
        public Statement apply(Statement statement, Description description) {
            return new Statement() {
                @Override
                public void evaluate() throws Throwable {
                    try {
                        statement.evaluate();
                    } finally {
                        tearDown();
                    }
                }
            };
        }
    }
}
