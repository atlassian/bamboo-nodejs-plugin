package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.result.JobResultViewLogs;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.exceptions.BambooSpecsPublishingException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.NpmTask;
import com.atlassian.bamboo.specs.model.task.NpmTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.testutils.UniqueNameHelper;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.atlassian.bamboo.testutils.user.TestUser;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for "npm" Bamboo task. Only contains non-trivial tests, as basic npm component is used in almost every other
 * test.
 */
public class NpmTaskTest extends AbstractNodeTaskTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Override
    protected void onSetUp() {
    }

    @Override
    protected void onTearDown() {
    }

    /**
     * Config constants for testing the npm task.
     */
    private interface TestConfig {
        String COMMAND = "install";
        boolean USE_ISOLATED_CACHE = true;
        String ENV_VARIABLES = "NODE_HOME=/home";
        String WORKING_SUBDIR = "dir";
    }

    private final List<PlanKey> plansToDelete = new ArrayList<>();

    @After
    public void cleanUp() {
        plansToDelete.forEach(backdoor.plans()::deletePlan);
        plansToDelete.clear();
    }

    @Test
    public void testTaskWithIsolatedCache() throws Exception {
        product.gotoLoginPage().loginAsSysAdmin();

        final TestBuildDetails plan = createAndSetupPlan();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // create two npm tasks using isolated cache
        final Map<String, String> npmInstallGrunt = ImmutableMap.of(
                NpmConfigurator.COMMAND, "install grunt",
                NpmConfigurator.ISOLATED_CACHE, Boolean.TRUE.toString());

        final Map<String, String> npmInstallGulp = ImmutableMap.of(
                NpmConfigurator.COMMAND, "install gulp",
                NpmConfigurator.ISOLATED_CACHE, Boolean.TRUE.toString());

        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install grunt", npmInstallGrunt);
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install gulp", npmInstallGulp);

        // execute and assert execution was successful
        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        // analyze logs - assert only one cache directory was used
        final PlanKey jobKey = plan.getDefaultJob().getKey();
        final PlanResultKey resultKey = PlanKeys.getPlanResultKey(jobKey, 1);
        final JobResultViewLogs logsPage = product.visit(JobResultViewLogs.class, resultKey);

        final String cacheUsageLogIndicator = getText("npm.isolatedCache.usage", StringUtils.EMPTY);
        assertThat("Should have found an i18n property", cacheUsageLogIndicator, not(isEmptyOrNullString()));
        assert cacheUsageLogIndicator != null;

        final Iterable<String> cacheUsagesInLog = Iterables.filter(logsPage.getLog(), Predicates.containsPattern(cacheUsageLogIndicator));

        final Set<String> usedCacheDirectories = new HashSet<>();
        for (String cacheUsage : cacheUsagesInLog) {
            // we extract only the cache directory - omit log timestamp and the indicator
            usedCacheDirectories.add(cacheUsage.substring(cacheUsage.indexOf(cacheUsageLogIndicator) + cacheUsageLogIndicator.length()));
        }

        assertThat("Should have used only one isolated cache directory for subsequent npm tasks", usedCacheDirectories, hasSize(1));
    }

    @Test
    public void testSpecsImportSuccessPath() throws Exception {
        final String nodeExecutableLabel = getExistingNodeCapabilityLabel();
        final NpmTask npmTask = getNpmTaskSpec(nodeExecutableLabel);
        final AnyTask anyTask = getNpmAnyTaskSpec(npmTask);
        final List<Task<?, ?>> npmTaskSpecsToTest = Arrays.asList(npmTask, anyTask);

        for (Task<?, ?> npmTaskSpec : npmTaskSpecsToTest) {
            // import plan spec with valid npm task
            final PlanKey jobKey = importNpmSpec(npmTaskSpec);

            // verify import was successful
            product.fastLogin(TestUser.ADMIN);
            final TestJobDetails mockedTestJobDetails = when(mock(TestJobDetails.class).getKey()).thenReturn(jobKey).getMock();
            final NpmTaskComponent npmTaskComponent = product.visit(JobTaskConfigurationPage.class, mockedTestJobDetails)
                    .getTask(NpmTaskComponent.TASK_NAME)
                    .edit(NpmTaskComponent.class);

            assertThat(npmTaskComponent.getNodeExecutable(), is(nodeExecutableLabel));
            assertThat(npmTaskComponent.getCommand(), is(TestConfig.COMMAND));
            assertThat(npmTaskComponent.isUseIsolatedCache(), is(TestConfig.USE_ISOLATED_CACHE));
            assertThat(npmTaskComponent.getEnvironmentVariables(), is(TestConfig.ENV_VARIABLES));
            assertThat(npmTaskComponent.getWorkingDirectory(), is(TestConfig.WORKING_SUBDIR));
        }
    }

    @Test
    public void testSpecsImportWithInvalidExecutableLabel() throws Exception {
        // import plan spec with invalid npm task
        final String nodeExecutableLabel = "invalid node.js executable label";
        expectedException.expect(BambooSpecsPublishingException.class);
        expectedException.expectMessage(containsString(nodeExecutableLabel));
        importNpmSpec(getNpmTaskSpec(nodeExecutableLabel));
    }

    /**
     * Imports a plan using Bamboo Specs with npm task.
     *
     * @param npmTask npm task to import
     * @return full job key of the plan, containing the npm task
     */
    private PlanKey importNpmSpec(@NotNull Task<?, ?> npmTask) {
        final String projectKeyStr = UniqueNameHelper.makeUniqueName("PROJ");
        final String planKeyStr = UniqueNameHelper.makeUniqueName("PLAN");
        final String jobKeyStr = "JOB1";
        final PlanKey planKey = PlanKeys.getPlanKey(projectKeyStr, planKeyStr);
        final PlanKey jobKey = PlanKeys.getJobKey(planKey, jobKeyStr);

        final Project projectSpec = new Project()
                .name(UniqueNameHelper.makeUniqueName("Project"))
                .key(projectKeyStr);
        final Plan planSpec = new Plan(projectSpec, UniqueNameHelper.makeUniqueName("Plan"), planKeyStr)
                .stages(new Stage("Stage")
                        .jobs(new Job("Job 1", jobKeyStr)
                                .tasks(npmTask)));

        final BambooServer bambooServer = getBambooServer(TestUser.ADMIN);
        bambooServer.publish(planSpec);
        plansToDelete.add(planKey);

        return jobKey;
    }

    /**
     * Get an instance of {@link NpmTask} with configuration from {@link TestConfig}.
     *
     * @param nodeExecutableLabel label of Node.js executable to use when configuring the task
     */
    private NpmTask getNpmTaskSpec(@NotNull String nodeExecutableLabel) {
        return new NpmTask()
                .nodeExecutable(nodeExecutableLabel)
                .command(TestConfig.COMMAND)
                .useIsolatedCache(TestConfig.USE_ISOLATED_CACHE)
                .environmentVariables(TestConfig.ENV_VARIABLES)
                .workingSubdirectory(TestConfig.WORKING_SUBDIR);
    }

    /**
     * Get an instance of {@link AnyTask} which represents the given {@link NpmTask}.
     *
     * @param npmTask {@link NpmTask} spec to mimic using {@link AnyTask}
     */
    private AnyTask getNpmAnyTaskSpec(@NotNull NpmTask npmTask) {
        final NpmTaskProperties npmTaskProperties = EntityPropertiesBuilders.build(npmTask);
        final AtlassianModule atlassianPlugin = new AtlassianModule(npmTaskProperties.getAtlassianPlugin().getCompleteModuleKey());
        return new AnyTask(atlassianPlugin)
                .configuration(ImmutableMap.of(
                        NpmConfigurator.NODE_RUNTIME, npmTaskProperties.getNodeExecutable(),
                        NpmConfigurator.COMMAND, npmTaskProperties.getCommand(),
                        NpmConfigurator.ISOLATED_CACHE, Boolean.toString(npmTaskProperties.isUseIsolatedCache()),
                        TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, StringUtils.defaultString(npmTaskProperties.getEnvironmentVariables()),
                        TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, StringUtils.defaultString(npmTaskProperties.getWorkingSubdirectory())));
    }
}
